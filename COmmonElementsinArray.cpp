/*
	Author: Suraj Dakua
	Date: 16-03-2020
	Description: Find common elements between two given array using single loop
*/

#include <iostream>
using namespace std;

//function to find common elements inside the arrray.
void commonElements(int a[], int b[], int size, int n)  //Function Signature.
{
	
	int key = 0;
	
	if (size < 2 && n < 2)
	{
		cout << "Insufficient data in Array." << endl;
	}

	for(int i = 0,j = 0; size > i && n > j;)
	{
		if (a[i] == b[j])
		{
			key = a[i];
			cout << "Common Element: " << key << endl;
			++i;
			++j;
		}
		
		else if (a[i] < b[j])
		{
			i++;
		}
		
		else
		{
			j++;
		}
	}
}

//Driver Code.
int main(void)
{
	int a[] = {1, 3, 4, 7, 10};
	int b[] = {2, 4, 7, 10, 12};
	
	int size = sizeof(a) / sizeof(a[0]);
	int n = sizeof(b) / sizeof(b[0]);
	commonElements(a, b, size, n);
}
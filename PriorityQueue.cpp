/*
	Author: Suraj Dakua
	Date: 19-03-2020
	Description: Priority Queue in C++
*/

#include<iostream>

int pQueue[100], maxSize, front, rear;
void initPriorityQueue(int size)
{
	maxSize = size;
	front = 0;
	rear = -1;
}

// This is ascending priority queue.
void enQueue(int e)
{
	rear++;
	pQueue[rear] = e;
	int temp;
	//We first sort the array in ascending order this is Ascending Priority Queue(APQ).
	// APQ is also known as Minimum Priority Queue (MinPQ).
	//Here the element with smallest number comes out first and vice versa for Descending Priority Queue(DPQ).
	// DPQ is also known as Maximum Priority Queue (MaxPQ).
	for (int i = front; i < rear; ++i)
	{
		for (int j = front; j < rear; ++j)
		{
			if (pQueue[j] > pQueue[j+1])
			{
				temp = pQueue[j];
				pQueue[j] = pQueue[j+1];
				pQueue[j+1] = temp;
			}
		}
	}
}

int deQueue()
{
	int temp = pQueue[front];
	front++;
	return temp;
}

int isFull()
{
	if (rear == maxSize - 1)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int isEmpty()
{
	if (front > rear)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void printQueue()
{
	for (int i = front; i <= rear; ++i)
	{
		std::cout << pQueue[i] << "\t" << std::endl;
	}
}

int main(void)
{
	int size, choice, element;
	std::cout << "Enter size: ";
	std::cin >> size;

	initPriorityQueue(size);
	do
	{
		std::cout << "1.EnQueue.\n"
				  << "2.DeQueue.\n"
				  << "3.PrintCircularQueue.\n"
				  << "0.Exit."
				  << std::endl;
		std::cin >> choice;

		switch (choice)
		{
		case 1:
			if (isFull() != 1)
			{
				std::cout << "Enter Element: ";
				std::cin >> element;
				enQueue(element);
			}
			else
			{
				std::cout << "Queue is Full." << std::endl;
			}
			break;
		case 2:
			if (isEmpty() != 1)
			{
				std::cout << "DeQueued Element: " << deQueue() << std::endl;
			}
			else
			{
				std::cout << "Queue is Empty." << std::endl;
			}
			break;
		case 3:
			if (isFull() == 1)
			{
				std::cout << " *************************************** " << std::endl;
				printQueue();
				std::cout << " *************************************** " << std::endl;
			}
			else
			{
				std::cout << "Queue is Empty." << std::endl;
			}
			break;
		case 0:
			std::cout << "Exiting Goodbye." << std::endl;
			break;
		default:
			std::cout << "OOPS Something went wrong.";
			break;
		}
	} while (choice != 0);
}
/*
	Author: Suraj Dakua
	Date: 07-04-2020
	Description: Selection Sort in C++.
*/

#include<iostream>

void selectionSort(int a[], int size)
{
	int i, m, j, min, pos, n, temp;
	//First print the unsorted array.
	std::cout << "Unsorted Array : ";
	for(i = 0; i < size; ++i)
	{
		std::cout << a[i] << " ";
	}
	std::cout << std::endl;

	for(m = 0; m < size-1; m++)
	{
		min = a[m];
		pos = m;	//index of i.
		for(j = m+1; j < size; j++)
		{
			if(a[j] > min)
			{
				min = a[j];
				pos = j;
			}
		}
		//Do swapping.
		temp = a[m];
		a[m] = a[pos];
		a[pos] = temp;
	}

	std::cout << "Sorted Array : ";
	for(n = 0; n < size; n++)
	{
		std::cout << a[n] << " ";
	}
	std::cout << std::endl;
}

int main(void)
{
	//Take array and calculate size of array.
	int a[] = {44, 10, 23, 77, 2, 99, 88, 110};
	int size = sizeof(a)/sizeof(a[0]);

	//Call function to sort the array.
	selectionSort(a, size);

	//exit status.
	return 0;
}

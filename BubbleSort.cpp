/*
	Author: Suraj Dakua
	Date: 17-03-2020
	Description: Bubble Sort in C++
*/

#include<iostream>

void BubbleSort(int a[], int size)
{
	int temp;
	for (int l = 0; l < size; ++l)
	{
		std::cout << "\nUnsorted Array: " << a[l];
	}

	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < size; ++j)
		{
			if (a[i] < a[j])
			{
				// Do swapping of Elements.
				temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}
		}
	}

	std::cout << std::endl;

	for (int k = 0; k < size; k++)
	{
		std::cout << "\nSorted Array: " << a[k];
	}
	
}

//Driver Code.
int main(void)
{
	int size;
	std::cout << "Enter size of Array: ";
	std::cin >> size;

	int a[100];		//Array of size 100.
	std::cout << "Enter Elements in Array: ";

	for (int i = 0; i < size; ++i)
	{
		std::cin >> a[i];
	}
	BubbleSort(a, size);
}
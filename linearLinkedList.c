/*
	Author : Suraj Dakua
	Date : 05/04/2020
	Description : Linear Linked List in C
*/

/*
	ADT of Linear Linked List.
1.Insertleft	2.InsertRight	3.DeleteLeft 4.DeleteRight
5.Print LL     6.DeleteSpecific    7.InsertSpecific.
8.Delete Specific 9.Search Specific.
*There is nothing LIFO or FIFO in Linked List.
Definition : Linked list is a collection of nodes arranged in interlinked list manner.
*/

#include<stdio.h>
#include<stdlib.h>

//Decalre a node.
struct node
{
	int data;		//Data Of Node
	struct node *next;	//Address of next node.
};

//Take a root node.
struct node *root;	//root is a alias for node.

//Init the Linked List.
void initLinkedList()
{
	root = NULL;	//List is empty.
}

//In linkedlist left-most node is known as root.
void insertLeft(int e)
{
	struct node *n;		//Declare a node to insert left.
	n = (struct node*)malloc(sizeof(struct node));
	n->data = e;
	n->next = NULL;
	if(root == NULL)
	{
		root = n;
	}
	else
	{
		n->next = root;
		root = n;
	}
}

//delete the left-most node.
void deleteLeft()
{
	struct node *temp;
	if(root == NULL)
	{
		printf("----------------------------------------------------------\n");
		printf("Empty List.\n");
		printf("----------------------------------------------------------\n");
	}
	else
	{
		temp = root;
		root = root->next;
		printf("----------------------------------------------------------\n");
		printf("Deleted Element : %d\n", temp->data);
		printf("----------------------------------------------------------\n");
		free(temp);
	}
}

//Insert - Right to Linked List.
void insertRight(int e)
{
	struct node *temp, *n;
	n = (struct node*)malloc(sizeof(struct node));
	n->data = e;
	n->next = NULL;
	if(root == NULL)
	{
		root = n;
	}
	else
	{
		temp = root;
		while(temp->next != NULL)
		{
			temp = temp->next;
		}
		temp->next = n;
	}
}

//Delete Right.
void deleteRight()
{
	struct node *temp, *temp1;
	if(root == NULL)
	{
		printf("--------------------------------------------------------\n");
		printf("Empty List.\n");
		printf("---------------------------------------------------------\n");
	}
	else
	{
		temp = root;
		temp1 = temp;
		while(temp->next != NULL)
		{
			temp1 = temp;
			temp = temp->next;
		}
		temp1->next = NULL;
		printf("-----------------------------------------------------------\n");
		printf("Deleted Element : %d\n", temp->data);
		printf("-----------------------------------------------------------\n");
		free(temp);
	}
}

//search specific element.
void searchSpecific(int k)
{
	struct node *temp;
	if(root == NULL)
	{
		printf("Empty list.\n");
	}
	else
	{
		temp = root;
		while(temp->data != k && temp->next != NULL)
		{
			temp = temp->next;
			
		}
		if(temp != NULL)
		{
			printf("Element found : %d\n", k);
		}
		else
		{
			printf("Element not found : %d\n", k);
		}
		
	}
}

//Insert specific Node in linked list.
void insertSpecific(int e, int p)
{
	struct node *n, *temp;
	n = (struct node*)malloc(sizeof(struct node));
	n->data = e;
	n->next = NULL;
	if(root == NULL)
	{
		root = n;
	}
	else
	{
		if(p == 0)
		{
			n->next = root;
			root = n;
		}
		else
		{
			temp = root;
			while(p > 0 && temp != NULL)
			{
				temp = temp->next;
				p--;
			}
			if(temp != NULL)	//Position Exists.
			{
				if(temp->next == NULL)	//Right-Most Node.
				{
					temp->next = n;
				}
				else	//For inserting inbetween.
				{
					n->next = temp->next;
					temp->next = n;
				}
			}
			else	//position not found.
			{
				printf("Position not found.\n");
			}
		}
	}
}

//Delete Specific Node.
void deleteSpecific(int k)
{
	struct node *temp, *temp2;
	if(root == NULL)
	{
		printf("Empty List.\n");
	}
	else
	{
		temp = root;
		temp2 = root;
		while(temp->data != k && temp->next != NULL)
		{
			temp2 = temp;
			temp = temp ->next;
		}
			if(temp->data == k)	//Data Exists.
			{	
				if(temp == root)
				{
					root = root -> next;
				}
				else if(temp->next == NULL)
				{
					temp2->next = NULL;
				}
				else
				{
					temp2->next = temp->next;
				}
				printf("Deleted Key : %d\n", temp->data);
				free(temp);
			}
			else
			{
				printf("Key not found : %d\n", k);
			}
	}
}


//print linked list.
void printLinkedList()
{
	struct node *temp;
	if(root == NULL)
	{
		printf("-----------------------------------------------\n");
		printf("Empty List.\n");
		printf("-----------------------------------------------\n");
	}
	else
	{
		temp = root;
		printf("------------------------------------------------\n");
		while(temp != NULL)
		{

			printf("Elements of List : %d\n", temp->data);
			temp = temp->next;
		}
		printf("------------------------------------------------\n");
	}
}

int main(void)
{
	int choice, element, position, key;
	printf("--------------------------------------------------------------------------------------------------------\n");
	printf("1.Insert Left.\t 2.Delete Left\t 3.Insert Right\t 4.Delete Right\t 5.Insert Specific 6.Delete Specific 7.Search Specific 8.Print List.\t 0.Exit.\n");
	printf("---------------------------------------------------------------------------------------------------------\n");
	initLinkedList();
	do
	{
		printf("------------------------------------------------------------------\n");
		printf("Enter choice : ");
		scanf("%d", &choice);
		printf("-------------------------------------------------------------------------------------\n");
		switch(choice)
		{
			case 1:
				printf("Enter element to insert left : ");
				scanf("%d", &element);
				insertLeft(element);
				break;
			case 2:
				deleteLeft();
				break;
			case 3:
				printf("Enter element to insert right-most : ");
				scanf("%d", &element);
				insertRight(element);
				break;
			case 4:
				deleteRight();
				break;
			case 5:
				printf("Enter Element : ");
				scanf("%d", &element);
				printf("-------------------------------------------------------------------\n");
				printf("Enter position : ");
				scanf("%d", &position);
				insertSpecific(element, position);
				printf("--------------------------------------------------------------------\n");
				break;
			case 6:
				printf("-------------------------------------------------------------------\n");
				printf("Enter key to delete specific : ");
				scanf("%d", &position);
				deleteSpecific(position);
				printf("--------------------------------------------------------------------\n");
				break;
			case 7:
				printf("--------------------------------------------------------------------\n");
				printf("Enter key to search specific : ");
				scanf("%d", &key);
				searchSpecific(key);
				printf("--------------------------------------------------------------------\n");
				break;
			case 8:
				printLinkedList();
				break;
			case 0:
				printf("-------------------------------------------------------------------\n");
				printf("Exiting\n");
				printf("--------------------------------------------------------------------\n");
				break;
			default:
				printf("--------------------------------------------------------------------\n");
				printf("Wrong Choice.\n");
				printf("--------------------------------------------------------------------\n");
				break;
		}

	}while(choice != 0);

	return 0;
}

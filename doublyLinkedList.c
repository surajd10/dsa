/*
	Author : Suraj Dakua.
	Date : 05-04-2020.
	Description : Doubly linked list in c.
*/

#include<stdio.h>
#include<stdlib.h>

//make node of type struct.
struct node
{
	int data;
	struct node *previous, *next;
};

struct node *root;

//Init Doublly Linked List.
void initDoubly()
{
	root = NULL;
}

//Insert Left.
void insertLeft(int e)
{
	struct node *n;
	n = (struct node*)malloc(sizeof(struct node*));
	n->data = e;
	n->next = n->previous = NULL;
	if(root == NULL)
	{
		root = n;
	}
	else
	{
		n->next = root;
		root -> previous = n;
		root = n;
	}

}

//Delete Left.
void deleteLeft()
{
	struct node *t;
	if(root == NULL)
	{
		printf("Empty List.\n");
	}
	else
	{
		t = root;
		root = root->next;
		root->previous = NULL;
		printf("Deleted Element : %d\n", t->data);
		free(t);
	}

}

//insert right.
void insertRight(int e)
{
	struct node *n, *t;
	n = (struct node*)malloc(sizeof(struct node));
	n->data =e;
	n->previous = n->next = NULL;
	if(root == NULL)
	{
		root = n;
	}
	else
	{
		t = root;
		while(t->next != NULL)
		{
			t = t -> next;
		}
		t->next = n;
		n->previous = t;
	}
}

//delete right.
void deleteRight()
{
	struct node *t, *t2;
	if(root == NULL)
	{
		printf("Empty List.\n");
	}
	else
	{
		t = root;
		while(t->next != NULL)
		{
			t = t->next;
		}
		t2 = t->previous;
		t2->next = NULL;
		printf("Deleted : %d\n", t->data);
		free(t);
	}

}

//search specific in doubly linked list.
void searchSpecific(int k)
{
	struct node *t;
	if(root == NULL)
	{
		printf("Empty List.\n");
	}
	else
	{
		t = root;
		//traverse through the list.
		while(t->data != k && t->next != NULL)
		{
			t = t->next;
		}
		if(t->data == k)
		{
			printf("Element found : %d\n", k);
		}
		else
		{
			printf("Element not found : %d\n", k);
		}
	}
}

//insert specific in doubly linked list.
void insertSpecific(int e, int p)
{
	struct node *t, *n;
	n = (struct node*)malloc(sizeof(struct node));
	n->previous = n->next = NULL;
	n->data = e;
	if(root == NULL)
	{
		printf("Empty List.\n");
	}
	else
	{
		if(p == 0)	//if position is zero i.e left most node.
		{
			n->next = root;
			root->previous = n;
			root = n;
		}
		else
		{
			t = root;
			//traverse through the list.
			while(p > 0 && t != NULL)
			{
				t = t->next;
				p--;
			}
			if(t != NULL)	//Position Exists.
			{
				if(t->next == NULL)	//for the right most node.
				{
					t->next = n;
				}
				else	//insert inbetween.
				{
					n->next = t->next;
					n->previous = t;
					t->next = n;
					t->next->previous = n;
					printf("Node Inserted.\n");
				}
			}
			else
			{
				printf("Position Not Found.\n");
			}
		}
	}
}

//delete specific node from linked list.
void deleteSpecific(int k)
{
	struct node *t, *t2;
	if(root == NULL)
	{
		printf("Empty List.\n");
	}
	else
	{
		t = root;
		t2= root;
		while(t->data != k && t->next != NULL)
		{
			t2 = t;
			t = t->next;
		}
		if(t->data == k)
		{
			if(t == root)	//Last Element Delete
			{
				root = root->next;
			}
			else if(t->next == NULL)	//Right-Most element delete.
			{
				t2 = t->previous;
				t2->next = NULL;
			}
			else	//Delete inbetween specified element.
			{
				t2->next = t->next;
				t->next->previous = t2;
			}
			printf("Deleted Node : %d\n", t->data);
			free(t);
		}
		else
		{
			printf("Node Data not found.\n");
		}
	}


}
//print doubly linked list
void printDoubly()
{
	struct node *t;
	if(root == NULL)
	{
		printf("Empty List.\n");
	}
	else
	{
		t= root;
		while(t != NULL)
		{
			printf("Data Elements : %d\n", t->data);
			t = t->next;
		}
	}
}

//print reverse.
void printReverseDoubly()
{
	struct node *t;
	if(root == NULL)
	{
		printf("Empty List.\n");
	}
	else
	{
		t= root;
		while(t->next != NULL)
		{
			t = t->next;
		}
		while(t != NULL)
		{
			printf("List in Reverse Order : %d\n", t->data);
			t = t->previous;
		}

	}
}


int main(void)
{
	int choice, element, key, position;
	printf("-----------------------------------------------------------------------------------------------------------------\n");
	printf("1.Insert Left.\t 2.Delete Left\t 3.Insert Right\t 4.Delete Right\n"); 
	printf("5.Print List.\t 6.Print Reverse\t 7.Search Specific\t 8.Insert Specific\t 9.Delete Specific\t 0.Exit.\n");
	printf("------------------------------------------------------------------------------------------------------------------\n");
	initDoubly();
	do
	{
		printf("------------------------------------------------------------------\n");
		printf("Enter choice : ");
		scanf("%d", &choice);
		printf("-------------------------------------------------------------------------------------\n");
		switch(choice)
		{
			case 1:
				printf("Enter element to insert left : ");
				scanf("%d", &element);
				insertLeft(element);
				break;
			case 2:
				deleteLeft();
				break;
			case 3:
				printf("Enter element to insert right-most : ");
				scanf("%d", &element);
				insertRight(element);
				break;
			case 4:
				deleteRight();
				break;
			case 5:
				printDoubly();
				break;
			case 6:
				printReverseDoubly();
				break;
			case 7:
				printf("------------------------------------------------------------------\n");
				printf("Enter key to search : ");
				scanf("%d", &key);
				searchSpecific(key);
				printf("-------------------------------------------------------------------\n");
				break;
			case 8:
				printf("--------------------------------------------------------------------\n");
				printf("Enter Element to Insert : ");
				scanf("%d",&element);
				printf("Enter position to insert : ");
				scanf("%d", &position);
				insertSpecific(element, position);
				printf("--------------------------------------------------------------------\n");
				break;
			case 9:
				printf("--------------------------------------------------------------------\n");
				printf("Enter Node to Delete : ");
				scanf("%d", &element);
				deleteSpecific(element);
				printf("----------------------------------------------------------------------\n");
				break;
			case 0:
				printf("-------------------------------------------------------------------\n");
				printf("Exiting\n");
				printf("--------------------------------------------------------------------\n");
				break;
			default:
				printf("--------------------------------------------------------------------\n");
				printf("Wrong Choice.\n");
				printf("--------------------------------------------------------------------\n");
				break;
		}

	}while(choice != 0);

	return 0;
}




/*
	Author: Suraj Dakua
	Date: 19-03-2020
	Description: Implementation of Circular Queue in C++
*/

#include<iostream>

//Advantage of Ciruclar Queue over Linear Queue is in Circular Queue you can reuse the space of the dequeued element.
int cQueue[100], front, rear, maxSize, count;
void initCircularQueue(int size)
{
	maxSize = size;		
	front = 0;
	count = 0;
	rear = -1;	//Circular Queue is Empty.
}

void enQueue(int e)
{
	rear = (rear + 1) % maxSize;		//Formula to rotate array in circular manner.
	cQueue[rear] = e;
	count++;			//Increment count whenever element is EnQueue.
}

int deQueue()
{
	int temp = cQueue[front];
	front = (front + 1) % maxSize;		//Formula to rotate array in ciruclar manner.
	count--;			//Decrement count whenever element is Dequeue.
	return temp;
}

int isFull()
{
	if (count == maxSize)		//Check whether count is equal is max size.
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int isEmpty()
{
	if (count == 0)		//Check whether count is zero.
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void printQueue()
{
	int i = front, cnt = count;
	while (cnt > 0)
	{
		std::cout << cQueue[i] << "\t\n";
		i = (i + 1) % maxSize;	
		cnt--;
	}

}

int main(void)
{
	int element, choice, size;
	std::cout << " ******************************************* " << std::endl;
	std::cout << "		Hola User.		" << std::endl;
	std::cout << " ********************************************* " << std::endl;
	std::cout << "Enter size: ";
	std::cin >> size;

	initCircularQueue(size);
	do
	{
		std::cout << "1.EnQueue.\n"
			   	  << "2.DeQueue.\n"
				  << "3.PrintCircularQueue.\n"
				  << "0.Exit."
				  << std::endl;
		std::cin >> choice;

		switch (choice)
		{
		case 1:
			if (isFull() != 1)
			{
				std::cout << "Enter Element: ";
				std::cin >> element;
				enQueue(element);
			}
			else
			{
				std::cout << "Queue is Full." << std::endl;
			}
			break;
		case 2:
			if (isEmpty() != 1)
			{
				std::cout << "DeQueued Element: " << deQueue() << std::endl;
			}
			else
			{
				std::cout << "Queue is Empty." << std::endl;
			}
			break;
		case 3:
			if (isFull() == 1)
			{
				std::cout << " *************************************** " << std::endl;
				printQueue();
				std::cout << " *************************************** " << std::endl;
			}
			else
			{
				std::cout << "Queue is Empty." << std::endl;
			}
			break;
		case 0:
			std::cout << "Exiting Goodbye." << std::endl;
			break;
		default:
			std::cout << "OOPS Something went wrong.";
			break;
		}
	} while (choice != 0);
}

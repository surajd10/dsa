/*
	Author: Suraj Dakua
	Date: 17-03-2020
	Description: Insertion Sort in C++
*/

#include<iostream>
using namespace std;

void insertionSort(int a[], int size)
{
	int temp, j;
	for (int m = 0; m < size; ++m)
	{
		cout << "Unsorted Array: " << a[m] << endl;
	}

	for (int i = 0; i < size; ++i)
	{
		temp = a[i + 1];
		j = i + 1;
		while (j > 0 && a[j - 1] > temp)
		{
			a[j] = a[j - 1];
			j--;  // Shift Back.
		}
		a[j] = temp;
	}

	for (int n = 1; n <= size; ++n)
	{
		cout << "Sorted Array: " << a[n] << endl;
	}
}

//Driver COde.
int main(void)
{
	int size;
	cout << "Enter size of Array: ";
	cin >> size;

	int a[100];
	cout << "Enter elements: ";
	for (int i = 0; i < size; ++i)
	{
		cin >> a[i];
	}
	insertionSort(a, size);
}
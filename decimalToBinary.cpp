/*
    Author : Suraj Dakua
    Date : 1-05-2020
    Description : Write a program to convert decimal to binary number.
*/

#include <iostream>
using namespace std;

int main(void)
{
    int dec{0};
    int binary[10];
    int counter{0};

    cout << "==========================================" << endl;
    cout << "Enter decimal number between 1 to 10 : ";
    cin >> dec;

    //
    for(;dec > 0;)
    {
        int digit  = dec % 2;   
        binary[counter] = digit;     
        dec = dec / 2;
        counter++;

    }

    //Print in reverse order.
    cout << "Binary Equivalent : ";
    for(counter = counter - 1; counter >= 0; counter--)
    {
        cout << binary[counter];        
    }

    cout << "\n==========================================" << endl;

    return 0;
}
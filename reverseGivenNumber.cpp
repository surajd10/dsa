/*
    Author : Suraj Dakua
    Date : 1-05-2020
    Description : Write a program to reverse a given number.
*/

#include <iostream>
using namespace std;

int main(void)
{
    int number {};
    int reversedNumber {0};

    //Ask user to enter a number.
    cout << "===================================" << endl;
    cout << "Enter a nummber to reverse : ";
    cin >> number;

    //Logic to reverse number.
    for(; number > 0;)
    {
        int digit = number % 10;    //Gives the last digit.
        reversedNumber = reversedNumber * 10 + digit;
        number = number / 10;
    }

    cout << "Reversed Number : " << reversedNumber <<  endl;
    cout << "==========================================" << endl;

    return 0;
}
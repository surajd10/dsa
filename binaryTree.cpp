/*
    Author : Suraj Dakua
    Date : 22/10/2020
    Description : Implement Binary Tree using C language.
                  Functionalities : Inorder/PostOrder/PreOrder/CountNodes.
*/
#include<iostream>
using namespace std;

// Create Node 
struct node
{
    /* data */
    int data;
    struct node *left, *right;
};

//Create Tree
struct node *createNode(int val) {
   struct node *temp = new node;
   temp->data = val;
   temp->left = temp->right = NULL;
   return temp;
}

//Insert Node in Tree
struct node* insertNode(struct node *r, int val)
{
    if(r == NULL)
        r = createNode(val);
    else{
        if(val < r->data)
        {
            if(r->left == NULL)
            {
                r->left = insertNode(r->left, val);
            }
            else
            {
                return insertNode(r->left, val);
            }
        }
        else
        {
            if(r->right == NULL)
            {
                r->right = insertNode(r->right, val);
            }
            else
            {
                return insertNode(r->right,val);
            }   
        }  
    }
}

/* 
 * inOrder Tree Traversal
 * sequence should be LPR( Left Parent Right)
 */
void inOrder(struct node *r)
{
    if(r != NULL)
    {
        inOrder(r->left);
        cout << r->data << endl;
        inOrder(r->right);
    }
}

/* 
 * PreOrder Tree Traversal
 * sequence should be PLR(parent Left Right)
 */
void preOrder(struct node *r)
{
    if(r != NULL)
    {
        cout << r->data << endl;
        preOrder(r->left);
        preOrder(r->right);
    }
}

/* 
 * PostOrder Tree Traversal
 * sequence should be LRP( Left Right Parent)
 */
void postorder(struct node *r)
{
    if(r != NULL)
    {
        postorder(r->left);
        postorder(r->right);
        cout << r->data << endl;
    }
}

int countNodes(struct node *r)
{
    if (r == NULL) 
       return 0; 
   
    int res = 0; 
    if  (r->left && r->right)  
       res++; 
   
    res += (countNodes(r->left) +  
            countNodes(r->right)); 
    return res; 
}

int getLeafCount(struct node* node)  
{  
    if(node == NULL)      
        return 0;  
    if(node->left == NULL && node->right == NULL)  
        return 1;          
    else
        return getLeafCount(node->left)+  
            getLeafCount(node->right);  
}  

int main(void)
{
    /* Create Tree Structure */
    struct node *root = NULL; //Here Root has NULL value or Tree is Empty.
    root = insertNode(root, 50); // Root node 

    /* Insert Data into Tree*/
    insertNode(root, 40);
    insertNode(root, 51);
    insertNode(root, 30);
    insertNode(root, 45);
    insertNode(root, 15);
    insertNode(root, 37);
    insertNode(root, 41);
    insertNode(root, 46);
    insertNode(root, 49);
    insertNode(root, 53);
    insertNode(root, 47);
    insertNode(root, 52);
    insertNode(root, 51);
    insertNode(root, 54);

    /* Inorder Print */
    cout << "Inorder Tree Traversal : " << endl;
    inOrder(root);
    
    /* Preorder Print */
    cout << "Preorder Tree Traversal : " << endl;
    preOrder(root);
    
    /* Postorder Print */
    cout << "Postorder Tree Traversal : " << endl;
    postorder(root);

    /* Count Nodes of Tree */
    cout << "Nodes Count : ";
    cout << countNodes(root) << endl;
    cout << "Leaf Count : " << getLeafCount(root) << endl;
	return 0;
}

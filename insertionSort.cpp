/*
	Author : Suraj Dakua
	Date : 22-04-2020
	Description : Insertion Sort in C++.
*/

#include<iostream>
using namespace std;

void insertionSort(int a[])
{
	cout << "Unsorted Array : "; 
	for(int i = 0; i < 4; ++i)
	{
		cout << a[i] << " ";
	}
	cout << endl;

	for(int j = 0; j < 3; ++j)
	{
		int ne = a[j+1];
		int m = j+1;
		while(m > 0 && a[m-1] > ne)
		{
			a[m] = a[m-1];
			m--;
		}
		a[m] = ne;
	}

	cout << "Sorted Array : ";
	for(int k = 0; k < 4; ++k)
	{
		cout << a[k] << " ";
	}
	cout << endl;
}

int main(void)
{
	int a[4] = {4,2,3,1};
	insertionSort(a);
	return 0;
}

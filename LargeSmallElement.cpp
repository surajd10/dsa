/*
	Author: Suraj Dakua
	Date: 16-03-2020
	Description: Find the second smallest and largest element in the given array using single for loop.
*/

#include<iostream>
using namespace std;
void findSecondLargestElement(int a[], int size)
{
	int first, second;
	//Check Array should have more than 2 elements.
	if (size < 2)
	{
		std::cout << "Insufficient data in array." << std::endl;
	}

	//If more than 2 elements enter inside loop.
	first = second = INT_MIN;  //Init memory to variables.
	for (int i = 0; i < size; ++i)
	{
		if (a[i] > first)
		{
			second = first;
			first = a[i];
		}

		else if (a[i] > second && a[i] != first)
		{
			second = a[i];
		}
	}

	if (second == INT_MIN) //Check if second element is == 0.
	{
		std::cout << "No second largest element found." << std::endl;
	}
	else
	{
		std::cout << "Second Largest Element in Array: " << second << std::endl;
	}

}

void findSecondSmallestElement(int a[], int size)
{
	int f, s;
	//Check Array should have more than 2 elements.
	if (size < 2)
	{
		std::cout << "Insufficient data in array." << std::endl;
	}

	//If more than 2 elements enter inside loop.
	f = s = INT_MAX; //Init memory to variables.
	for (int i = 0; i < size; ++i)
	{
		if (a[i] < f)
		{
			s = f;
			f = a[i];
		}

		else if (a[i] < s && a[i] != f)
		{
			s = a[i];
		}
	}
	if (s == INT_MAX)
	{
		cout << "No second smallest element." << endl;
	}
	else
	{
		cout << "Second smallest Element in Array: " << s << endl;
	}
		
}

int main(void)
{
	int a[] = {3, 4, 1, 12, 7, 9};
	int size = sizeof(a) / sizeof(a[0]);
	findSecondLargestElement(a,size);
	findSecondSmallestElement(a, size);
}
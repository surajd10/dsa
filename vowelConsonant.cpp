/*
    Author : Suraj Dakua
    Date : 1-05-2020
    Description : Write a program to check whether a character is vowel or consosnant.
*/

#include <iostream>
using namespace std;

//Driver Code.
int main(void)
{
    //Create array of vowels.
    char array[10] = {'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'};
    char array[10] = {'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'};
    char input{};

    //Enter char to check whether it is vowel/consonant
    cout << "==================================================" << endl;
    cout << "Enter a char : ";
    cin >> input;

    //Traverse through array.
    for(int i = 0; i < 10; ++i)
    {
        //If found print and return from the main function.
        if(array[i] == input)
        {
            cout << "Entered char is vowel : " << input << endl;
            cout << "==================================================" << endl;
            return 0;
        }
    }

    //If not found then print it is consonant.
    cout << "Entered char is consonant : " << input << endl;
    cout << "==================================================" << endl;

    return 0;   //Exit status.
}
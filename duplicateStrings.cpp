/*
    Author : Suraj Dakua
    Date : 2-05-2020
    Description : Write a program to find duplicates from a string.
*/

#include<iostream>
#include<string>
#include<cstring>
using namespace std;

int main(void)
{
    string str;
    cout << "Enter string to find duplicates : ";
    getline(cin,str);

    //Find the length of the string.
    int x = str.length();

    //Convert the string into character array first and then compare the characters using two for loops.
    //Declare a char array.
    char charArray[x+1];
    strcpy(charArray, str.c_str());

    cout << "Duplicate Character :[ ";

    for(size_t i = 0; i < x; i++)
    {
        //Take another loop to compare the ith element with i+1th element.
        for(size_t j = i + 1; j < x; j++)
        {
            if(charArray[i] == charArray[j])
            {
                cout << charArray[i];
                break;
            }
        }
    }

    cout << " ]";
    cout << endl;
    
    return 0;
}
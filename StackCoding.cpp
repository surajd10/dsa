 /*
    Author: Suraj Dakua
    Date: 17-03-2020
    Description: Basic Stack Initiliazation and Operations in C++.
 */

#include<iostream>
using namespace std;

int Stack[100], maxSize, tos;   // tos = top of stack.

// Function to Initialize Stack.
void initStack(int size)
{
    maxSize = size;
    tos = -1;    // Initially Stack is empty because index of stack starts at 0.
}

// If not full start pushing elements on stack.
void push(int e)
{
    tos++;  // Increment tos because it is at index -1 by incrementing it will go to index 0 which is starting index of stack.
    Stack[tos] = e;
}

// Check whether stack is Full
int isFull()
{
    if (tos == maxSize - 1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

// Pop element from stack.
int pop()
{
    int temp = Stack[tos];  // Returns the last element pushed into stack because stack is LIFO.
    tos--;
    return temp;
}

// Checks whether the stack is empty.
int isEmpty()
{
    if (tos == -1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

// Returns the element top of stack not neccessary function as pop does the same task.
int atTOS()
{
    return Stack[tos];
}

// Print the stack in LIFO pattern.
void printStack()
{
    //Should print the elements in LIFO pattern thats why i starts with tos and go's upto i = 0.
    for (int i = tos; i >= 0; i--)  
    {
        cout << "Elements of Stack: " << Stack[i] << endl;
    }
}

// Driver Code.
int main(void)
{
    int size, choice, element;
    
    cout << "------------------------------------------------------------" << endl;
    cout << "Hello User." << endl;
    cout << "------------------------------------------------------------" << endl;
    cout << "Enter size of Stack: ";
    cin >> size;
    cout << "------------------------------------------------------------" << endl;

    initStack(size);
    do
    {
        cout  << "1.Push\t"
              << "2.Pop\t"
              << "3.Element on ToS.\t"
              << "4.Print Stack.\t"
              << "0.Exit"
              << endl;
        cin >> choice;
        cout << "--------------------------------------------------------------" << endl;
        switch (choice)
        {
        case 1:
            if (isFull() == 0)
            {
                cout << "Enter Element: ";
                cin >> element;
                push(element);
            }
            else
            {
                cout << "Stack is Full." << endl;
            }
            break;
        case 2:
            if (isEmpty() == 0)
            {
                cout << "Poped Element: " << pop() << endl;
            }
            else
            {
                cout << "Can't pop because stack is Empty." << endl;
            }
            break;
        case 3:
            if (isEmpty() != 1)
            {
                cout << "Top element of stack: " << atTOS() << endl;
            }
            else
            {
                cout << "Stack is Empty." << endl;
            }
            break;
        case 4:
            if (isFull() == 1)
            {
                printStack();
            }
            else
            {
                cout << "Stack is Empty." << endl;
            }
            break;
        case 0:
            cout << "Exiting Goodbye." << endl;
            break;
        default:
            cout << "OOPS I think you entered wrong choice." << endl;
            cout << "----------------------------------------------------------------" << endl;
            break;
        }
    } while (choice != 0);
} // Close the do while loop.

/*
    Author : Suraj Dakua
    Date : 02-05-2020
    Description : Write a program to check whether the two strings are rotation of each other.
*/

#include<string>
#include<iostream>
#include<cstring>

using namespace std;

// function definition.
void checkRotation(string s, string s1)
{
    //store the original string in another variable.
    string dummyStr = s;
    string dummyStr1 = s1;

    //find lengths of the strings
    int len = dummyStr.length();
    int len2 = dummyStr1.length();

    //Perform reverse operation on the copy of first string.
    for(int i = 0, j = len -1; i < len / 2; ++i, --j)
    {
        char temp = dummyStr[i];
        dummyStr[i] = dummyStr[j];
        dummyStr[j] = temp;
    }

    //Perform reverse operation on the copy of second string.
    for(int m = 0, n = len2 -1; m < len2 / 2; ++m, --n)
    {
        char temp = dummyStr1[m];
        dummyStr1[m] = dummyStr1[n];
        dummyStr1[n] = temp;
    }

    cout << dummyStr << endl;
    cout << dummyStr1 << endl;

    if(dummyStr == s1 && dummyStr1 == s)
    {
        cout << "Congrats! Both the strings are rotations of each other." << endl;
    }

    else
    {
        cout << "They are not rotation of each other." << endl;
    }
}

int main(void)
{
    string str1, str2;
    cout << "============================================" << endl;
    cout << "Enter string 1 : ";
    getline(cin, str1);

    cout << "Enter string 2 :";
    getline(cin, str2);

    checkRotation(str1, str2);
    cout << "============================================" << endl;
    return 0;
}
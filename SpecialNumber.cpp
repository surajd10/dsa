/*
	Author: Suraj Dakua
	Date: 18-03-2020
	Description: In a given array find whether the elements inside array are special number or not.
*/

#include<iostream>
using namespace std;

int isSpecialNumber(int x)
{
	int sumofDigits = 0, temp = 0;
	temp = x;
	for (; temp > 0;)
	{
		int fact = 1;
		for (int j = 1; j <= temp % 10; j++)
		{
			fact = fact * j;
				
		}

		sumofDigits = sumofDigits + fact;
		temp = temp / 10;
	}
	return sumofDigits;
}

void func(int a[], int size)
{
	for (int n = 0; n < size; ++n)
	{
		cout << "Given Array : " << a[n] << endl;
	}

	cout << endl;

	for (int m = 0; m < size; ++m)
	{
		isSpecialNumber(a[m]);
		if (a[m] == isSpecialNumber(a[m]))
		{
			cout << "Special Number : " << a[m] << endl;
		}

		else
		{
			cout << "Not a special Number : " << a[m] << endl;
		}
	}
}

int main(void)
{
	int a[] = {145, 135, 2, 175};
	int size = sizeof(a) / sizeof(a[0]);
	func(a, size);
}

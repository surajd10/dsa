/*
	Author: Suraj Dakua
	Date : 19-03-2020
	Description: Implementation of Linear Queue i.e. Most Basic type of Queue in C++
*/

#include<iostream>
using namespace std;

int Queue[100], front, rear, maxSize;

void initQueue(int size)
{
	maxSize = size;
	rear = -1;		// Queue is empty.
	front = 0;		// Since there is no element in the queue.
}

void enQueue(int e)
{
	rear++;		// First increment queue 
	Queue[rear] = e;	// Then enqueue the element in Queue.
}

int deQueue()
{
	int temp = Queue[front];
	front++;
	return temp;
}

int isFull()
{
	if (rear == maxSize - 1)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int isEmpty()
{
	if (front > rear)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void printQueue()
{
	for (int i = front; i <= rear; ++i)
	{
		cout << Queue[i] << "\t" << endl;
	}
}


int main(void)
{
	int element, choice, size;
	cout << " ******************************************** " << endl;
	cout << "	Hola User.	" << endl;
	cout << " ******************************************** " << endl;
	cout << "Enter Size of Queue: ";
	cin >> size;
	cout << " ******************************************** " << endl;

	initQueue(size);
	do
	{
		cout << "1.EnQueue.\n"
			 << "2.DeQueue.\n"
			 << "3.Print Queue.\n"
			 << "0.Exit." << endl;
		cout << " **************************************** " << endl;
		cin >> choice;

		switch (choice)
		{
		case 1:
			if (isFull() == 0)
			{
				cout << "Enter Element : ";
				cin >> element;
				enQueue(element);
			}
			else
			{
				cout << "Queue is Full." << endl;
			}
			break;
		case 2:
			if (isFull() == 0)
			{
				cout << "Element on the Front: " << deQueue() << endl;
			}
			else
			{
				cout << "Cannot dequeue sinnce Queue is empty." << endl;
			}
			break;
		case 3:
			if (isFull() == 1)
			{
				printQueue();
			}
			else
			{
				cout << "Cannot print since Queue is empty." << endl;
				cout << " ********************************************* " << endl;
			}
			break;
		case 0:
			cout << "Exiting from the Queue." << endl;
			break;
		default:
			cout << "OOPS! Something went wrong." << endl;
			break;
		}
	} while (choice != 0);
}

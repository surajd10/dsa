/*
	Author: Suraj Dakua
	Date: 27-03-2020
	Description: Tower of Hanoi using Recursion.
*/

#include<iostream>
using namespace std;

void towerOfHanoi(int num, char from_peg, char to_peg, char auxilary_peg)
{
	// If their is one disk directly move it to destination needle.
	if (num == 1)
	{
		cout << "Move Disk " << num << " from " << from_peg << " to " << to_peg << endl;
		return;
	}

	// If disk is not equal to one then use recursion.
	towerOfHanoi(num - 1, from_peg, auxilary_peg, to_peg);
	cout << "Move disk " << num << " from " << from_peg << "to " << to_peg << endl;
	towerOfHanoi(num - 1, auxilary_peg, to_peg, from_peg);	
}

int main(void)
{
	int num;
	cout << "Enter number of Disk : ";
	cin >> num;

	towerOfHanoi(num, 'A', 'C', 'B');
}


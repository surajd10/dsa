/*
	Author: Suraj Dakua
	Date: 17-03-2020
	Desription: Find the clumps in a given array.
*/

#include <iostream>
using namespace std;

void findClumps(int a[], int size)
{
	int count = 0;
	bool flag = false;
	int currentElement = a[0];

	for (int i = 1; i < size; ++i)
	{
		if (currentElement == a[i])
		{
			if (!flag)  // !false = true.
			{
				count++;
				flag = true;
			}
		}
		
		else
		{
			currentElement = a[i];
			flag = false;
		}
	}
	cout << "Clumps in given Array: " << count << endl;
}

//Driver Code.
int main(void)
{
	int a[] = { 1, 1, 2, 3, 3, 4, 5, 5};
	int size = sizeof(a) / sizeof(a[0]);
	findClumps(a, size);
}
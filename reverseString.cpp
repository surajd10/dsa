/*
    Author : Suraj Dakua
    Date : 2-05-2020
    Description : Write a program to reverse a given string.
*/

#include<iostream>
#include<string>
#include<cstring>
using namespace std;

int main(void)
{
    string str;
    cout << "Enter string to reverse : ";
    getline(cin,str);

    int len = str.length();
    cout << len << endl;

    //Iterate through the char array and repalce the ith and jth position through normal swap operation.
    for(int i = 0, j = len - 1; i < len / 2; ++i, --j)
    {
        //Perform swapping of character positions.
        char temp = str[i];
        str[i] = str[j];
        str[j] = temp;
    }

    cout << str << endl;

    return 0;
}